<?php
/**
 * Template part for displaying mappings grid
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mapping
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<a class="gotomapping" href="' . get_the_permalink() . '"><h1 class="entry-title">', '</h1></a>' ); ?>
	</header><!-- .entry-header -->

	<?php mapping_post_thumbnail(); ?>

	<div class="entry-content">
		<?php the_excerpt(); ?>
		<a class="gotomapping" href="<?php the_permalink(); ?>">Acceder Mapping</a>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
