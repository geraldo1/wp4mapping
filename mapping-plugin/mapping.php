<?php
/*
Plugin Name: Mapping
Description: Custom post types for project FAVB Mapping
Author: Gerald Kogler
Author URI: https://geraldo.github.io
Text Domain: mapping
*/

/**
 * load textdomain
 */
add_action( 'init', 'mapping_load_textdomain' );
function mapping_load_textdomain() {
	load_plugin_textdomain('mapping', false, dirname(plugin_basename(__FILE__)));
}

/**
 * remove default post type
 */
add_action('admin_menu','mapping_remove_default_post_type');
function mapping_remove_default_post_type() {
	remove_menu_page('edit.php');
}

function mapping_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'mapping_login_logo_url' );


/**
 * remove dashbaord access
 */
add_action('init', 'mapping_remove_dashboard_access');
function mapping_remove_dashboard_access() {
    if ( is_admin() ) {
        
        // exit if doing Ajax
        if (defined( 'DOING_AJAX' ) && DOING_AJAX )
            return;

        if (strpos( $_SERVER[ 'REQUEST_URI' ], 'wp-admin/admin-ajax.php' ) !== false)
            return;

        // exit if media upload is in process
        if (strpos( $_SERVER[ 'REQUEST_URI' ], 'wp-admin/media-upload.php' ) !== false)
            return;

        if (strpos( $_SERVER[ 'REQUEST_URI' ], 'wp-admin/async-upload.php' ) !== false)
            return;

        // Define capabilities allowed to cotrol restrict backend. 
        $mapping_access_roles = array('manage_options', 'edit_others_posts', 'publish_posts', 'edit_posts', 'read');
        
        if ( !current_user_can('edit_others_posts') ) {
            //error_log("exit restrict");
            wp_redirect( home_url() );
            exit;
        }
    }
}

/**
 * remove admin toolbar
 */
add_action('after_setup_theme', 'mapping_remove_admin_bar');
function mapping_remove_admin_bar() {
	if (!current_user_can('administrator') && !current_user_can('editor') && !is_admin()) {
		
		show_admin_bar(false);
	}
}

/**
 * redirect after login and logout
 */
add_filter('login_redirect', 'mapping_default_page', 10, 3);
add_filter('logout_redirect', 'mapping_default_page', 10, 3);
function mapping_default_page($redirect, $request, $user) {
	return ( home_url() );
}

/**
 * Add custom post types to 'At a Glance' widget
 */
function mapping_add_custom_post_counts() {
   $post_types = array('poi', 'mapping', 'mapping-category');  // array of custom post types to add to 'At A Glance' widget
   foreach ($post_types as $pt) :
      $pt_info = get_post_type_object($pt); // get a specific CPT's details
      $num_posts = wp_count_posts($pt); // retrieve number of posts associated with this CPT
      $num = number_format_i18n($num_posts->publish); // number of published posts for this CPT
      $text = _n( $pt_info->labels->singular_name, $pt_info->labels->name, intval($num_posts->publish) ); // singular/plural text label for CPT
      echo '<li class="page-count '.$pt_info->name.'-count"><a href="edit.php?post_type='.$pt.'">'.$num.' '.$text.'</a></li>';
   endforeach;
}
add_action('dashboard_glance_items', 'mapping_add_custom_post_counts');

/**
 * define custom post type ITINERARI
 */
function mapping_register_poi() {

    register_post_type( 'mapping-category', array(
        'labels' => array(
            'name' => 'Mapping Categories',
            'singular_name' => 'Mapping Category',
            'menu_name' => 'MappingCategories',
            'all_items' => 'Tots MappingCategories',
            'edit_item' => 'Edita Mapping Category',
            'view_item' => 'View Mapping Category',
            'view_items' => 'View MappingCategories',
            'add_new_item' => 'Afegeix nou Mapping Category',
            'add_new' => 'Afegeix nou Mapping Category',
            'new_item' => 'Nou Mapping Category',
            'parent_item_colon' => 'Parent Mapping Category:',
            'search_items' => 'Cerca MappingCategories',
            'not_found' => 'No mappingcategories found',
            'not_found_in_trash' => 'No mappingcategories found in Trash',
            'archives' => 'Mapping Category Archives',
            'attributes' => 'Mapping Category Attributes',
            'insert_into_item' => 'Insert into mapping category',
            'uploaded_to_this_item' => 'Uploaded to this mapping category',
            'filter_items_list' => 'Filter mappingcategories list',
            'filter_by_date' => 'Filter mappingcategories by date',
            'items_list_navigation' => 'MappingCategories list navigation',
            'items_list' => 'MappingCategories list',
            'item_published' => 'Mapping Category published.',
            'item_published_privately' => 'Mapping Category published privately.',
            'item_reverted_to_draft' => 'Mapping Category reverted to draft.',
            'item_scheduled' => 'Mapping Category scheduled.',
            'item_updated' => 'Mapping Category updated.',
            'item_link' => 'Mapping Category Link',
            'item_link_description' => 'A link to a mapping category.',
        ),
        'public' => true,
        'show_in_rest' => true,
        'supports' => array(
            0 => 'title',
        ),
        'delete_with_user' => false,
    ) );

    register_post_type( 'mapping', array(
        'labels' => array(
            'name' => 'Mappings',
            'singular_name' => 'Mapping',
            'menu_name' => 'Mappings',
            'all_items' => 'Tots Mappings',
            'edit_item' => 'Edita Mapping',
            'view_item' => 'View Mapping',
            'view_items' => 'View Mappings',
            'add_new_item' => 'Afegeix nou Mapping',
            'add_new' => 'Afegeix nou Mapping',
            'new_item' => 'Nou Mapping',
            'parent_item_colon' => 'Parent Mapping:',
            'search_items' => 'Cerca Mappings',
            'not_found' => 'No mappings found',
            'not_found_in_trash' => 'No mappings found in Trash',
            'archives' => 'Mapping Archives',
            'attributes' => 'Mapping Attributes',
            'insert_into_item' => 'Insert into mapping',
            'uploaded_to_this_item' => 'Uploaded to this mapping',
            'filter_items_list' => 'Filter mappings list',
            'filter_by_date' => 'Filter mappings by date',
            'items_list_navigation' => 'Mappings list navigation',
            'items_list' => 'Mappings list',
            'item_published' => 'Mapping published.',
            'item_published_privately' => 'Mapping published privately.',
            'item_reverted_to_draft' => 'Mapping reverted to draft.',
            'item_scheduled' => 'Mapping scheduled.',
            'item_updated' => 'Mapping updated.',
            'item_link' => 'Mapping Link',
            'item_link_description' => 'A link to a mapping.',
        ),
        'public' => true,
        'show_in_rest' => true,
        'supports' => array(
            0 => 'title',
            1 => 'editor',
        ),
        'delete_with_user' => false,
    ) );

    register_post_type( 'poi', array(
        'labels' => array(
            'name' => 'POIs',
            'singular_name' => 'POI',
            'menu_name' => 'POIs',
            'all_items' => 'Todos POIs',
            'edit_item' => 'Editar POI',
            'view_item' => 'Ver POI',
            'view_items' => 'Ver POIs',
            'add_new_item' => 'Añadir nuevo POI',
            'new_item' => 'Nuevo POI',
            'parent_item_colon' => 'POI superior:',
            'search_items' => 'Buscar POI',
            'not_found' => 'No se han encontrado POIs',
            'not_found_in_trash' => 'No hay POIs en la papelera',
            'archives' => 'Archivo de POIs',
            'attributes' => 'Atributos de POIs',
            'insert_into_item' => 'Insertar en POI',
            'uploaded_to_this_item' => 'Subido a este POI',
            'filter_items_list' => 'Filtrar lista de POI',
            'filter_by_date' => 'Filtrar POIs por fecha',
            'items_list_navigation' => 'Navegación por la lista de POIs',
            'items_list' => 'Lista de POIs',
            'item_published' => 'POI publicado.',
            'item_published_privately' => 'POI publicado de forma privada.',
            'item_reverted_to_draft' => 'POI devuelto a borrador.',
            'item_scheduled' => 'POIs programados.',
            'item_updated' => 'POI actualizado.',
            'item_link' => 'Enlace a POI',
            'item_link_description' => 'Un enlace a un POI.',
        ),
        'public' => true,
        'show_in_rest' => true,
        'supports' => array(
            0 => 'title',
            1 => 'editor',
            2 => 'thumbnail',
            3 => 'author',
        ),
        'delete_with_user' => false,
    ) );

    register_taxonomy( 'poitag', array(
        0 => 'poi',
    ), array(
        'labels' => array(
            'name' => 'poiTags',
            'singular_name' => 'poiTag',
            'menu_name' => 'poiTags',
            'all_items' => 'Todos poiTags',
            'edit_item' => 'Editar poiTag',
            'view_item' => 'Ver poiTag',
            'update_item' => 'Actualizar poiTag',
            'add_new_item' => 'Añadir nuevo poiTag',
            'new_item_name' => 'Nombre del nuevo poiTag',
            'search_items' => 'Buscar poiTags',
            'popular_items' => 'poiTags populares',
            'separate_items_with_commas' => 'Separa los poitags con comas',
            'add_or_remove_items' => 'Añadir o quitar poitags',
            'choose_from_most_used' => 'Elige entre los poitags más usados',
            'not_found' => 'No se han encontrado poitags',
            'no_terms' => 'No hay poitags',
            'items_list_navigation' => 'Navegación por la lista de poiTags',
            'items_list' => 'Lista de poiTags',
            'back_to_items' => '← Ir a poitags',
            'item_link' => 'Enlace a poiTag',
            'item_link_description' => 'Un enlace a un poitag',
        ),
        'public' => true,
        'show_in_menu' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
    ) );
}
add_action( 'init', 'mapping_register_poi' );

/**
 * add capabilities for type POI
 */
function mapping_add_cap_upload() {
	// roles:
	// WP admin
	// WP editor: edit everything, manage content and users from back end
	// author: publish new POIs and edit own, front end only

	// admin
	$role = get_role( 'administrator' );

    $role->add_cap( 'edit_pois' ); 
    $role->add_cap( 'edit_published_pois' ); 
    $role->add_cap( 'edit_private_pois' ); 
    $role->add_cap( 'edit_others_pois' ); 
    $role->add_cap( 'publish_pois' ); 
    $role->add_cap( 'read_private_pois' ); 
    $role->add_cap( 'delete_private_pois' ); 
    $role->add_cap( 'delete_pois' ); 
    $role->add_cap( 'delete_published_pois' ); 
    $role->add_cap( 'delete_others_pois' );

	// editor
	$role = get_role( 'editor' );

    $role->add_cap( 'edit_pois' ); 
    $role->add_cap( 'edit_published_pois' ); 
    $role->add_cap( 'edit_private_pois' ); 
    $role->add_cap( 'edit_others_pois' );
    $role->add_cap( 'publish_pois' ); 
    $role->add_cap( 'read_private_pois' ); 
    $role->add_cap( 'delete_pois' ); 
    $role->add_cap( 'delete_private_pois' ); 
    $role->add_cap( 'delete_published_pois' ); 
    $role->add_cap( 'delete_others_pois' );
    $role->add_cap( 'create_users' );
    $role->add_cap( 'list_users' );
    $role->add_cap( 'edit_users' );
    $role->add_cap( 'list_users' );
    $role->add_cap( 'delete_users' );
    $role->add_cap( 'remove_users' );
    //$role->add_cap( 'add_users' );
    //$role->add_cap( 'promote_users' );

    $role->add_cap( 'edit_user' ); 

    $role = get_role( 'author' );
    $role->add_cap( 'upload_files' ); 
}
add_action( 'init', 'mapping_add_cap_upload' );

/*function mapping_add_roles_on_plugin_activation() {
   add_role( 'master', 'Master', get_role( 'editor' )->capabilities );
}
register_activation_hook( __FILE__, 'mapping_add_roles_on_plugin_activation' );*/

add_action( 'acf/include_fields', function() {
    if ( ! function_exists( 'acf_add_local_field_group' ) ) {
        return;
    }

    acf_add_local_field_group( array(
    'key' => 'group_656e626ab19e8',
    'title' => 'Mapping',
    'fields' => array(
        array(
            'key' => 'field_656e626b36ead',
            'label' => 'Lugar',
            'name' => 'place',
            'aria-label' => '',
            'type' => 'map_field',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'center_lat' => 41.40,
            'center_lng' => 2.15,
            'zoom' => 12,
        ),
        array(
            'key' => 'field_6576c46aaa4e5',
            'label' => 'Coordinadas',
            'name' => 'coordinates',
            'aria-label' => '',
            'type' => 'group',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'layout' => 'block',
            'sub_fields' => array(
                array(
                    'key' => 'field_6576c4baaa4e7',
                    'label' => 'Longitude',
                    'name' => 'longitude',
                    'aria-label' => '',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => 'map-field-input-lng',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'min' => '',
                    'max' => '',
                    'placeholder' => '',
                    'step' => '',
                    'prepend' => '',
                    'append' => '',
                ),
                array(
                    'key' => 'field_6576c4a2aa4e6',
                    'label' => 'Latitude',
                    'name' => 'latitude',
                    'aria-label' => '',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => 'map-field-input-lat',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'min' => '',
                    'max' => '',
                    'placeholder' => '',
                    'step' => '',
                    'prepend' => '',
                    'append' => '',
                ),
            ),
        ),
        array(
            'key' => 'field_657639ccd68f3',
            'label' => 'Color',
            'name' => 'color',
            'aria-label' => '',
            'type' => 'color_picker',
            'instructions' => 'Color de fondo del mapa y del ícono en vista inicial.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'enable_opacity' => 0,
            'return_format' => 'string',
        ),
        array(
            'key' => 'field_656e62e936eae',
            'label' => 'Conectar puntos con línea',
            'name' => 'paint_track',
            'aria-label' => '',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Mostrar línea conectando los POIs en orden de fecha&hora de publicación.',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_6570157b8ee67',
            'label' => 'Audio',
            'name' => 'add_audio',
            'aria-label' => '',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Mostrar campo de audio',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_657018249a19b',
            'label' => 'Íconos',
            'name' => 'pois_icons',
            'aria-label' => '',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Mostrar POIs con íconos en vez de círculos de color.',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_657015b38ee68',
            'label' => 'Categorías',
            'name' => 'tags',
            'aria-label' => '',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'layout' => 'table',
            'pagination' => 0,
            'min' => 0,
            'max' => 0,
            'collapsed' => '',
            'button_label' => 'Añadir una fila',
            'rows_per_page' => 20,
            'sub_fields' => array(
                array(
                    'key' => 'field_6570179b8ee69',
                    'label' => 'Categoría del mapping',
                    'name' => 'mapping_category',
                    'aria-label' => '',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array(
                        0 => 'mapping-category',
                    ),
                    'post_status' => array(
                        0 => 'publish',
                    ),
                    'taxonomy' => '',
                    'return_format' => 'object',
                    'multiple' => 0,
                    'allow_null' => 0,
                    'bidirectional' => 1,
                    'bidirectional_target' => array(
                        0 => 'field_659d6f4c4f1bf',
                    ),
                    'ui' => 1,
                    'parent_repeater' => 'field_657015b38ee68',
                ),
                /*array(
                    'key' => 'field_657017ab8ee6a',
                    'label' => 'Color',
                    'name' => 'color',
                    'aria-label' => '',
                    'type' => 'color_picker',
                    'instructions' => 'Sobreescribe color por defiición de esta categoría.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'enable_opacity' => 0,
                    'return_format' => 'string',
                    'parent_repeater' => 'field_657015b38ee68',
                ),
                array(
                    'key' => 'field_657017d88ee6b',
                    'label' => 'Ícono',
                    'name' => 'icon',
                    'aria-label' => '',
                    'type' => 'image',
                    'instructions' => 'Sobreescribe ícono por definición de esta categoría.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                    'preview_size' => 'medium',
                    'parent_repeater' => 'field_657015b38ee68',
                ),*/
            ),
        ),
        array(
            'key' => 'field_657018249a222',
            'label' => 'Etiquetas',
            'name' => 'has_tags',
            'aria-label' => '',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Los usuarios pueden añadir etiquetas libremente.',
            'default_value' => 1,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'mapping',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
    'show_in_rest' => 1,
) );

    acf_add_local_field_group( array(
    'key' => 'group_657639cb8e207',
    'title' => 'Mapping Categories',
    'fields' => array(
        array(
            'key' => 'field_657639ccd68e7',
            'label' => 'Color',
            'name' => 'color',
            'aria-label' => '',
            'type' => 'color_picker',
            'instructions' => 'Color por definición, puede ser sobreescrita cuando se elija en un mapping.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'enable_opacity' => 0,
            'return_format' => 'string',
        ),
        array(
            'key' => 'field_6576c15488469',
            'label' => 'Icon',
            'name' => 'icon',
            'aria-label' => '',
            'type' => 'image',
            'instructions' => 'Ícono por definición, puede ser sobreescrita cuando se elija en un mapping.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
            'preview_size' => 'medium',
        ),
        array(
            'key' => 'field_659d6f4c4f1bf',
            'label' => 'Mappings',
            'name' => 'mappings',
            'aria-label' => '',
            'type' => 'relationship',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'post_type' => array(
                0 => 'mapping',
            ),
            'post_status' => array(
                0 => 'publish',
            ),
            'taxonomy' => '',
            'filters' => '',
            'return_format' => 'object',
            'min' => '',
            'max' => '',
            'elements' => '',
            'bidirectional' => 0,
            'bidirectional_target' => array(
            ),
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'mapping-category',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
    'show_in_rest' => 1,
) );

    acf_add_local_field_group( array(
    'key' => 'group_653bf9a0885e2',
    'title' => 'POIs',
    'fields' => array(
        array(
            'key' => 'field_659d753ce1030',
            'label' => 'Mapping',
            'name' => 'mapping',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'post_type' => array(
                0 => 'mapping',
            ),
            'post_status' => array(
                0 => 'publish',
            ),
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 0,
            'allow_null' => 0,
            'bidirectional' => 0,
            'ui' => 1,
            'bidirectional_target' => array(
            ),
        ),
        array(
            'key' => 'field_65881487b4229',
            'label' => 'Categoría',
            'name' => 'category',
            'aria-label' => '',
            'type' => 'relationship',
            'instructions' => 'En este lugar...',
            'required' => 1,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_659d753ce1030',
                        'operator' => '!=empty',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'post_type' => array(
                0 => 'mapping-category',
            ),
            'post_status' => array(
                0 => 'publish',
            ),
            'taxonomy' => '',
            'filters' => array(
                0 => 'search',
            ),
            'return_format' => 'object',
            'min' => 1,
            'max' => 1,
            'elements' => '',
            'bidirectional' => 0,
            'bidirectional_target' => array(
            ),
        ),
        array(
            'key' => 'field_653bf9fb1d117',
            'label' => 'Etiquetas',
            'name' => 'poi_tags',
            'aria-label' => '',
            'type' => 'taxonomy',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'taxonomy' => 'poitag',
            'add_term' => 1,
            'save_terms' => 1,
            'load_terms' => 0,
            'return_format' => 'object',
            'field_type' => 'multi_select',
            'allow_null' => 0,
            'bidirectional' => 0,
            'multiple' => 0,
            'bidirectional_target' => array(
            ),
        ),
        array(
            'key' => 'field_653bfa3c87494',
            'label' => 'Imagen',
            'name' => 'image',
            'aria-label' => '',
            'type' => 'image',
            'instructions' => 'Haz una foto y nos lo muestras',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'library' => 'uploadedTo',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
            'preview_size' => 'medium',
        ),
        array(
            'key' => 'field_659ecdc68dc0e',
            'label' => 'Audio',
            'name' => 'audio',
            'aria-label' => '',
            'type' => 'file',
            'instructions' => 'Graba un audio y cuéntanos porqué. El audio pueda tener una duración máxima de 30 segundos. ',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'uploadedTo',
            'min_size' => '',
            'max_size' => '5MB',
            //'mime_types' => 'mp3, ogg, wma, m4a, wav, aac, ac3, m4a, flac, aiff, aa, aax, au, pcm, opus',
        ),
        array(
            'key' => 'field_653d10989d3e5',
            'label' => 'Mapa',
            'name' => 'map',
            'aria-label' => '',
            'type' => 'map_field',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'center_lat' => '41.4',
            'center_lng' => '2.15',
            'zoom' => 18,
        ),
        array(
            'key' => 'field_653c28a0bd0bd',
            'label' => 'Coordinadas',
            'name' => 'coordinates',
            'aria-label' => '',
            'type' => 'group',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'layout' => 'table',
            'sub_fields' => array(
                array(
                    'key' => 'field_653c28c0bd0bf',
                    'label' => 'Longitude',
                    'name' => 'longitude',
                    'aria-label' => '',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => 'map-field-input-lng',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'min' => '',
                    'max' => '',
                    'placeholder' => '',
                    'step' => '',
                    'prepend' => '',
                    'append' => '',
                ),
                array(
                    'key' => 'field_653c28aebd0be',
                    'label' => 'Latitude',
                    'name' => 'latitude',
                    'aria-label' => '',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => 'map-field-input-lat',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'min' => '',
                    'max' => '',
                    'placeholder' => '',
                    'step' => '',
                    'prepend' => '',
                    'append' => '',
                ),
            ),
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'poi',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
    'show_in_rest' => 1,
) );
} );

/*
 * Filter POI Rest API by custom ACF field mapping
 */
add_filter( 'rest_query_vars', function ( $valid_vars ) {
    return array_merge( $valid_vars, array( 'mapping_id', 'meta_query' ) );
} );
add_filter( 'rest_poi_query', function( $args, $request ) {
    $mapping_id = $request->get_param( 'mapping_id' );

    if ( ! empty( $mapping_id ) ) {
        $args['meta_query'] = array(
            array(
                'key'     => 'mapping',
                'value'   => $mapping_id,
                'compare' => '=',
            )
        );      
    }

    return $args;
}, 10, 2 );

/*
 * Filter mapping categories by mapping_id
 */
add_filter('acf/fields/relationship/query/name=category', 'mapping_acf_fields_relationship_query', 10, 3);
function mapping_acf_fields_relationship_query( $args, $field, $post_id ) {

    //$mapping_id = get_field('mapping', $post_id);
    //$mapping_id = $_GET['mapping_id'];

    // limit to actual mapping
    //$args['meta_key'] = 'mapping';
    //$args['meta_value_num'] = 527;
    $args['meta_query'] = array(
        array(
            'key' => 'mappings',
            'value' => 527,
            'compare' => 'LIKE',
        )
    );

    // Restrict results to children of the current post only.
    $args['post_parent'] = $post_id;

    return $args;
}

/*
 * Add audio files mime types
 */
function mapping_mime_types( $mimes ) {
        $mimes['m4a'] = 'audio/mp4';
        $mimes['mp4'] = 'audio/mp4';
        return $mimes;
}
add_filter( 'upload_mimes', 'mapping_mime_types' );
?>