<?php
/**
 * mapping functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mapping
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mapping_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on mapping, use a find and replace
		* to change 'mapping' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'mapping', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'mapping' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'mapping_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'mapping_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mapping_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mapping_content_width', 640 );
}
add_action( 'after_setup_theme', 'mapping_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mapping_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'mapping' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'mapping' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'mapping_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mapping_scripts() {
	wp_enqueue_style( 'mapping-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'mapping-style', 'rtl', 'replace' );

	wp_enqueue_style( 'xrcb-font-style', get_template_directory_uri() . '/css/general-sans.css', array(), _S_VERSION);
	wp_enqueue_style( 'xrcb-custom-style', get_template_directory_uri() . '/css/custom.css', array(), filemtime(get_template_directory() . '/css/custom.css'));
	wp_enqueue_style( 'xrcb-map-style', get_template_directory_uri() . '/css/map.css', array(), filemtime(get_template_directory() . '/css/map.css'));

	wp_enqueue_script( 'mapping-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	wp_register_script('ol-js', "https://cdn.jsdelivr.net/npm/ol@v8.1.0/dist/ol.js", array( 'jquery' ), '8.1.0', true);
	wp_enqueue_script('ol-js');
	wp_register_style('ol-style', "https://cdn.jsdelivr.net/npm/ol@v8.1.0/ol.css", array(), '8.1.0');
	wp_enqueue_style('ol-style');

	wp_register_script('ol-ext', "https://cdn.jsdelivr.net/npm/ol-ext@4.0.13/dist/ol-ext.min.js", array( 'ol-js' ), '4.0.13', true);
	wp_enqueue_script('ol-ext');

	wp_register_script('ol-layerswitcher', "https://cdn.jsdelivr.net/npm/ol-layerswitcher@4.1.0/dist/ol-layerswitcher.min.js", array( 'ol-js' ), '4.1.0', true);
	wp_enqueue_script('ol-layerswitcher');
	wp_register_style('ol-layerswitcher-style', "https://cdn.jsdelivr.net/npm/ol-layerswitcher@4.1.0/dist/ol-layerswitcher.min.css", array(), '4.1.0');
	wp_enqueue_style('ol-layerswitcher-style');

	wp_enqueue_script( 'map-js', get_template_directory_uri() . '/js/map.js', array('ol-js'), filemtime(get_template_directory() . '/js/map.js'), true );

	/* ajaxifying theme */
	if ( ! is_admin() ) {

		/* include jquery $.browser code to work in WP 5.6 */
		wp_enqueue_script( 'browser', get_template_directory_uri() . '/js/browser.js', array(), '20201012', true );

		wp_enqueue_script( 'hash-change', get_template_directory_uri() . '/js/jquery.ba-hashchange.min.js', array('jquery'), '', true);
		wp_enqueue_script( 'ajax-theme', get_template_directory_uri() . '/js/ajax.js', array( 'hash-change' ), filemtime(get_template_directory() . '/js/ajax.js'), true);
	}
}
add_action( 'wp_enqueue_scripts', 'mapping_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
