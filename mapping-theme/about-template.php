<?php
/**
 * Template Name: About
 *
 * @package mapping
 */

get_header();

wp_localize_script('map-js', 'map_js_vars', null);
?>

	<div id="primary" class="content-area page">

		<div id="content" class="site-content" role="main">
			<div class="header">
				<?php the_content(); ?>
				<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>
			</div>

			<div class="articles">

				<?php 
				// wp-query to get all published posts without pagination
				$allPostsWPQuery = new WP_Query(array('post_type'=>'mapping', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
				 
				<?php if ( $allPostsWPQuery->have_posts() ) : ?>
				 
					<?php while ( $allPostsWPQuery->have_posts() ) : 

					     	$allPostsWPQuery->the_post();
					        get_template_part( 'template-parts/content', 'mappings' );

					endwhile; ?>
				
					<?php wp_reset_postdata(); ?>
				 <?php else : ?>
				    <p><?php _e( 'There no posts to display.' ); ?></p>
				<?php endif; ?>

			</div>
		</div>
	</div>

<?php
get_footer();
