<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mapping
 */

wp_localize_script('map-js', 'map_js_vars', array('mapping_id' => get_the_ID()));
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php mapping_post_thumbnail(); ?>

	<div class="entry-content">
		<?php the_content(); ?>

		<p>Categorías: 
		<?php 
			$categories = get_field('tags');
        	if ($categories) {
        		forEach($categories as $i=>$category) {
	        		if ($i>0) echo ', ';
	        		echo $category["mapping_category"]->post_title;
		    	}
		    }
		?>
		</p>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php mapping_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
