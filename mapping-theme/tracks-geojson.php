<?php
/**
	Template Name: Tracks GeoJSON
*/

header('Content-Type: application/json; charset=utf-8');
//header('Content-Disposition: attachment; filename='export.csv'');
$fp = fopen('php://output', 'w');

$features = array(
	'features' => array(),
	'type' => 'FeatureCollection'
);

if (isset($_GET['mapping_id'])) {
	$mapping_id = $_GET['mapping_id'];

	$users = get_users( array(
		//'show_fullname' => 1,
		//'orderby'       => 'post_count',
		//'order'         => 'DESC',
	) );

	foreach ( $users as $user ) {

		$query_args = array(
			'post_type' => 'poi',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby' => 'date',
			'order' => 'ASC',
			'meta_key' => 'mapping',
			'meta_value' => $mapping_id,
			'author' => $user->ID
		);

		$my_query = new WP_Query($query_args);

		if ( $my_query->have_posts() ) {

			$coords = [];
			while ($my_query->have_posts()) {

				$my_query->the_post();

				$coords[] = array(
					(float)get_field('coordinates')['longitude'],
					(float)get_field('coordinates')['latitude']
				);
			}

			if (count($coords) > 1) {

				$features['features'][] = array(
					'id' => (int)$user->ID,
					'properties' => array(
						'user_id' => (int)$user->ID,
						'user_name' => $user->user_nicename,
						'type' => 'track',
					),
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'LineString',
						'coordinates' => $coords
					)
				);
			}
		}
	}
}

echo json_encode($features);

fclose($fp);

?>