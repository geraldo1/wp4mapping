# Wordpress4Mapping

Simple solution for basic mapping software based on Wordpress. This was created for mobile use to go out on the street and map POIs. 

It uses the following plugins and libraries:
- [ACF](https://wordpress.org/plugins/advanced-custom-fields/) for forms
- [Openlayers](https://openlayers.org/) for maps

Custom development includes:
- Theme [mapping](https://gitlab.com/geraldo1/wp4mapping/-/tree/main/mapping-theme?ref_type=heads) based on [undescores](https://underscores.me/)
- Plugin [mapping](https://gitlab.com/geraldo1/wp4mapping/-/tree/main/mapping-plugin?ref_type=heads) to register fields, taxonomies and CPT poi.
- Plugin [acf-map-field](https://gitlab.com/geraldo1/acf-map-field/) to include map in form.
