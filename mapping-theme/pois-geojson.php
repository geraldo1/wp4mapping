<?php
/**
	Template Name: POIs GeoJSON
*/

if (isset($_GET['mapping_id'])) {
	$mapping_id = $_GET['mapping_id'];
}

header('Content-Type: application/json; charset=utf-8');
//header('Content-Disposition: attachment; filename='export.csv'');
$fp = fopen('php://output', 'w');

$features = [];

$query_args = array(
	'post_type' => 'poi',
	'post_status' => 'publish',
	'posts_per_page' => -1
);

// filter by mapping
if (isset($mapping_id)) {
	$query_args['meta_key'] = 'mapping';
	$query_args['meta_value'] = $mapping_id;
}

$my_query = new WP_Query($query_args);

if ( $my_query->have_posts() ) {

	$features = array(
		'features' => array(),
		'type' => 'FeatureCollection'
	);

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$category = get_field('category')[0];
		$category->color = get_field('color', $category->ID);
		$category->icon = get_field('icon', $category->ID);

		$features['features'][] = array(
			'id' => (int)get_the_ID(),
			'properties' => array(
				'id' => (int)get_the_ID(),
				'title' => get_the_title(),
				'tag' => get_field('tags'),
				'category' => $category,
				'type' => 'poi',
				'permalink' => get_permalink(get_the_ID()),
			),
			'type' => 'Feature',
			'geometry' => array(
				'type' => 'Point',
				'coordinates' => array(
					(float)get_field('coordinates')['longitude'],
					(float)get_field('coordinates')['latitude']
				)
			)
		);
	}
}

echo json_encode($features);

fclose($fp);

?>