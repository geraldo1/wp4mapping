<?php
/**
 * @package mapping
 */

$mapping_id = get_field('mapping')->ID;
wp_localize_script('map-js', 'map_js_vars', array('mapping_id' => $mapping_id));
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<h1 class="entry-title">Espai <?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php if ( has_post_thumbnail() ) the_post_thumbnail('full'); ?>
			<?php //itinerari_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">

		<div class="entry-content-mapping">
			<?php the_content(); ?>

			<?php if (get_field('image')): ?>
				<img src="<?php echo get_field('image'); ?>"/>
			<?php endif; ?>

			<?php if (get_field('audio')):
				echo wp_audio_shortcode(array('src' => get_field('audio')['url']));
			endif; ?>

			<p>Categoría: 
			<?php 
				/*$tags = get_field('tags');
	        	if ($tags) {
		        	forEach($tags as $i=>$tag) {
		        		if ($i>0) echo ', ';
		        		echo get_term($tag, 'poitag')->name;
			    	}
			    }*/
			    echo get_field('category')[0]->post_title;
			?>
			</p>

			<p>Etiquetas: 
			<?php 
				$tags = get_field('tags');
	        	if ($tags) {
		        	forEach($tags as $i=>$tag) {
		        		if ($i>0) echo ', ';
		        		echo get_term($tag, 'poitag')->name;
			    	}
			    }
			?>
			</p>

			<!--<div class="line line1">
				<a class='btn-mapping' href='#' data-lat='<?php if (!empty($coordinates)) echo $coordinates["latitude"]; ?>' data-lon='<?php if (!empty($coordinates)) echo $coordinates["longitude"]; ?>'><img src="/mapping/wp-content/plugins/acf-map-field/assets/images/marker.png"></a>
			</div>-->

		</div>
	</div>

	<footer class="entry-meta">
		<?php //edit_post_link( __( 'Edit', 'mapping' ), '<span class="edit-link">', '</span>', $post->ID ); ?>
		<span class="edit-link"><a class="post-edit-link" href="/new-poi/?post_id=<?php the_ID(); ?>&mapping_id=<?php echo $mapping_id; ?>">Edit</a></span>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->