<?php
/**
 * Template name: Home page
 *
 * @package mapping
 */

get_header();

//wp_enqueue_script( 'map-js', get_template_directory_uri() . '/js/homemap.js', array('ol-js'), '1.0', true );
wp_localize_script('map-js', 'map_js_vars', null);
?>

	<div id="primary" class="content-area page">
	</div>

<?php
get_footer();
