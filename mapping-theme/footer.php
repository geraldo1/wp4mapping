<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mapping
 */

?>
    </div><!-- #main -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
jQuery(function() {
    // goto about on first visit
    if (document.location.pathname === "/" && !window.mobilecheck())
        document.location.href = "/sobre-el-proyecto";
});
</script>

</body>
</html>
