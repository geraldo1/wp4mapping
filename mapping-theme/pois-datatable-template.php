<?php
/**
 * Template Name: POIs Datatable
 *
 * @package mapping
 */

$poiUrl = '/wp-json/wp/v2/poi';

if (isset($_GET['mapping_id'])) {
	$mapping_id = $_GET['mapping_id'];
	$poiUrl .= '?mapping_id=' . $mapping_id;
	wp_localize_script('map-js', 'map_js_vars', array('mapping_id' => $mapping_id));
}

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

				<div id="datatable"></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="poi-table"></table>');
		var datatable = $('#poi-table').DataTable({
			paging: true,
			pageLength: 10,
			info: false,
			responsible: true,
			searching: false,	// to do! pass search param with ajx call and filter it at server side
			stateSave: false,
			serverSide: true,
			ajax: function ( data, callback, settings ) {
				$.ajax({
	                url: '<?php echo $poiUrl; ?>',
	                type: 'get',
	                contentType: 'application/x-www-form-urlencoded',
	                data: {
	                    offset: data.start,
	                    per_page: data.length
	                },
	                success: function( data, textStatus, jQxhr ){
	                	let totalRecords = jQxhr.getResponseHeader('x-wp-total');
	                    callback({
	                        data: data,
	                        recordsTotal: totalRecords,
	                        recordsFiltered: totalRecords
	                    });
	                },
	                error: function( jqXhr, textStatus, errorThrown ){
	                }
	            });
	        },
			columns: [
				{ "data": "title.rendered", "title" : "Título", "className" : "name", "render": function ( data, type, row ) {
						return "<a class='btn-poi' href='"+row["link"]+"' data-lat='"+row["acf"].coordinates.latitude+"' data-lon='"+row["acf"].coordinates.longitude+"'>"+data+"</a>";
					},
				},
				/*{ "data": "acf.category", "title" : "Category", "className" : "category", "render": function ( data, type, row, meta ) {
						let currentCell = $("#poi-table").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
						$.getJSON( '/wp-json/wp/v2/mapping-category/' + data, function(category) {
					  		$(currentCell).html("<span class='"+category.slug+"' style='background-color:"+category.acf.color+"'>"+category.title.rendered+"</span>");
						});
						return null;
					}
				},*/
				/*{ "data": "acf.poi_tags", "title" : "Etiqueta", "className" : "tag", "render": function ( data, type, row, meta ) {
						let currentCell = $("#poi-table").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
						if (data.length > 0)
							$.getJSON( '/wp-json/wp/v2/poitag/' + data, function(tag) {
						  		$(currentCell).html("<span class='"+tag.slug+"'>"+tag.name+"</span>");
							});
						return null;
					}
				},*/
				{ "data": "date", "title" : "Fecha", "className" : "date", "render": function ( data, type, row ) { 
						return new Date(data).toLocaleDateString('es-ES', {
							year: 'numeric',
							month: 'numeric',
							day: '2-digit',
						}) 
					} 
				},
				{ "data": "date", "title" : "Hora", "className" : "date", "render": function ( data, type, row ) { 
						return new Date(data).toLocaleTimeString('es-ES', {
							hour: 'numeric',
							minute: 'numeric'
						}) 
					} 
				},
				{ "data": "acf.coordinates.latitude", "title" : "Latitude", "className" : "lat" },
				{ "data": "acf.coordinates.longitude", "title" : "Longitude", "className" : "lon" }
			],
		}).on( 'init.dt', function () {

		    /*$("a.btn-poi").click(function(event){
		        //event.preventDefault();
		        map.getView().animate({ center: ol.proj.fromLonLat([$(this).data("lon"), $(this).data("lat")]), zoom: 16 });
		    });*/
		});

	});
</script>

<?php get_footer(); ?>
