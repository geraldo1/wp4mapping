const dir = '',
//const dir = '/mapping',
		  extentBarcelona = [210000, 5050000, 270000, 5090000];
let mapping_id = null;

// Calculation of resolutions that match zoom levels 12-20
// 156543.03392804097 = resolution at zoom level 0
var resolutions = [];
for (var i = 0; i <= 20; ++i) {
  resolutions.push(156543.03392804097 / Math.pow(2, i));
}

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];

	location.search
	.substr(1)
	.split("&")
	.forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

/*
* Define custom zoomtoextent control
*/
class ZoomExtentControl extends ol.control.Control {

  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement('button');
    button.innerHTML = options.label;
    //button.extent = options.extent;
    button.coords = options.coords;

    const element = document.createElement('div');
    element.className = 'zoom-extent custom-control ol-unselectable ol-control extent-' + options.label;
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });

    button.addEventListener('click', this.handleZoomExtent.bind(this), false);
  }

  handleZoomExtent(evt) {
  	jQuery(".close-button").click();

    /*this.getMap().getView().fit(evt.currentTarget.extent, {
			padding: [100, 100, 100, 300],
			duration: 2000,
			easing: ol.easing.easeOut
		});*/

		//console.log(evt.currentTarget.coords);
		this.getMap().getView().animate({ 
			center: ol.proj.fromLonLat(evt.currentTarget.coords),
			zoom: 12
		});
  }
}

/*
* Custom control to open Wordpress pages
*/
class OpenPageControl extends ol.control.Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement('button');
    button.innerHTML = options.label;
    button.url = options.url;

    const element = document.createElement('div');
    element.className = 'open-link custom-control ol-unselectable ol-control button-' + options.pos;
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });

    button.addEventListener('click', this.handleOpenLink.bind(this), false);
  }

  handleOpenLink(evt) {
    //console.log("open", evt.currentTarget.url);
    //window.open(evt.currentTarget.url, "_self");
    let link = document.createElement('a');
    link.href = evt.currentTarget.url;
    document.body.appendChild(link);
    link.click();
  }
}

/*
* Custom control to show/hide track line
*/
class PaintTrackControl extends ol.control.Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const input = document.createElement('input');
    input.type = "checkbox";
    input.id = "paintLine";

    var label = document.createElement('label')
		label.htmlFor = "paintLine";
		label.appendChild(document.createTextNode(options.label));

    const element = document.createElement('div');
    element.className = 'paint-line custom-control ol-unselectable ol-control';
    element.appendChild(input);
    element.appendChild(label);

    super({
      element: element,
      target: options.target,
    });

    input.addEventListener('change', this.showLine.bind(this), false);
  }

  showLine(evt) {
    //console.log("change", document.getElementById("paintLine").checked);
    trackSource.refresh();
  }
}

/*
* Base layers
*/
const coastLayer = new ol.layer.Vector({
	title: 'Costa',
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		//url: dir + '/wp-content/themes/mapping/data/coastline.json'
		url: dir + '/wp-content/themes/mapping/data/sea.json'
	}),
	style: new ol.style.Style({
		/*stroke: new ol.style.Stroke({ 
    	color: 'white', 
    	width: 1
    })*/
    fill: new ol.style.Fill({
    	color: '#2994ff'
    })
	}),
	maxZoom: 15
});

const districtsLayer = new ol.layer.Vector({
	title: 'Districtes',
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: dir + '/wp-content/themes/mapping/data/districts.json'
	}),
	style: districtsStyleFunction,
	maxZoom: 15
});

function districtsStyleFunction(feature, resolution) {
	let fillStyle = new ol.style.Fill({
  	color: '#aaa'
  });

	// highlight Sant Martí
	if (feature.get("N_Distri") === "Sant Martí") {
		fillStyle = new ol.style.Fill({
    	color: '#555'
    });
	}

	return new ol.style.Style({
		stroke: new ol.style.Stroke({ 
    	color: 'white', 
    	width: 3
    }),
    fill: fillStyle
	});
}

const neighbourhoodsLayer = new ol.layer.Vector({
	title: 'Barris',
	source: new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: dir + '/wp-content/themes/mapping/data/neighbourhoods.json'
	}),
	style: new ol.style.Style({
		stroke: new ol.style.Stroke({ 
    	color: 'white', 
    	width: 2,
    	lineDash: [.1, 5]
    })
	}),
	minZoom: 13,
	maxZoom: 18
});

const baseLayer = new ol.layer.Tile({ 
  source: new ol.source.XYZ({ 
    url:'https://{1-4}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
  }),
	minZoom: 15,
	maxZoom: 18
});

const baseLayerGlobal = new ol.layer.Tile({ 
  source: new ol.source.XYZ({ 
    url:'https://{1-4}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
  }),
	minZoom: 0,
	maxZoom: 11
});

let trackSource = null;

/*
* Init
*/
if (findGetParameter("mapping_id")) {
	mapping_id = findGetParameter("mapping_id");
}
else {
	try { 
		map_js_vars;

		if (map_js_vars.hasOwnProperty("mapping_id"))
			mapping_id = map_js_vars.mapping_id;
	}
	catch(e) {
    if(e.name == "ReferenceError") {
      throw new Error("Something went badly wrong!");
    }
	}
}

if (mapping_id) {
	console.log("mapping_id", mapping_id);

	jQuery(document).ready(function($) {
		// color or icon
		$.getJSON( '/wp-json/wp/v2/mapping/' + mapping_id, function(json) {
			if (json.hasOwnProperty('id')) {
				loadMapping(json);
			}
			else {
				loadHome();
			}
		});
	});

	trackSource = new ol.source.Vector({
		format: new ol.format.GeoJSON(),
		url: dir + '/tracks-geojson/?mapping_id=' + mapping_id
	});
}
else {
	loadHome();
}

/*
* Specific Mapping
*/
function loadMapping(mapping) {
	/*
	* Base map
	*/
	const mappingCoords = [mapping.acf.coordinates.longitude, mapping.acf.coordinates.latitude],
				mappingPaintTrack = mapping.acf.paint_track;

	const view = new ol.View({
		center: ol.proj.fromLonLat(mappingCoords),
		zoom: 12.5,
		minZoom: 12,
		maxZoom: 18,
		showFullExtent: true,
	  //extent: extentBarcelona
	});

	const map = new ol.Map({
    target: 'map',
		controls: ol.control.defaults.defaults().extend([
			new ZoomExtentControl({
	    	label: "Inicio: " + mapping.title.rendered,
	    	//extent: extentSanMarti
	    	coords: mappingCoords
	    }),
			new OpenPageControl({
				pos: 0,
	    	label: "Lista puntos",
	    	url: "/lista-puntos/?mapping_id=" + mapping_id
	    }),
			new OpenPageControl({
				pos: 1,
	    	label: "Añadir nuevo punto",
	    	url: "/new-poi/?mapping_id=" + mapping_id
	    }),
	    new PaintTrackControl({
				label: "Mostrar línea"
	    }),
		]),
    layers: [
    	coastLayer,
    	baseLayer,
    	baseLayerGlobal,
    	districtsLayer,
    	neighbourhoodsLayer,
    ],
    view: view
	});

	// set map background color
	jQuery("#map").css("background-color", mapping.acf.color);

	/*
	* Vector Layer Styles
	*/
	let poiStyle = new ol.style.Style({
	  	image: new ol.style.Circle({
	  		stroke: new ol.style.Stroke({ 
		    	color: "#fff", 
		    	width: 4
		    }),
		    fill: new ol.style.Fill({
		    	color: '#000'
		    }),
	        radius: 12
		})
	});
	let poiHighlightStyle = new ol.style.Style({
	  	image: new ol.style.Circle({
	        stroke: new ol.style.Stroke({ 
		    	color: "#3c3c3d", 
		    	width: 4
		    }),
	        fill: new ol.style.Fill({
	        	color: "#3c3c3d"
	        }),
	        radius: 14
		})
	});

	/*
	* add layer POIs
	*/
	let poiLayer = new ol.layer.Vector({
    title: 'POIs',
    type: 'poi',
    /*source: new ol.source.Cluster({
    	distance: 100,
  		minDistance: 50,*/
			source: new ol.source.Vector({
				format: new ol.format.GeoJSON(),
				url: dir + '/pois-geojson/?mapping_id=' + mapping_id
			}),
		//}),
		style: poiStyleFunction
  }),
	selectedFeature = null;
	map.addLayer(poiLayer);

	function poiStyleFunction(feature, resolution) {
		if (feature.get("category")) {
			let category = feature.get("category"),
					poiStyleCustom = null,
					color = "#000",
					icon = "/wp-content/plugins/acf-map-field/assets/images/marker.png";

			if (mapping.acf.pois_icons) {
				if (category.icon !== false)
					icon = category.icon.url;

				poiStyleCustom = new ol.style.Style({
	        image: new ol.style.Icon(({
	          anchor: [0.5, 36],
	          anchorXUnits: 'fraction',
	          anchorYUnits: 'pixels',
	          opacity: 0.75,
	          src: icon
	        }))
	      });
			}
			else {
				if (category.color !== "")
					color = category.color;

				//let size = feature.get('features').length;

				// cluster
				poiStyleCustom = new ol.style.Style({
			  	image: new ol.style.Circle({
			      radius: 12,
			  		stroke: new ol.style.Stroke({ 
				    	color: "#fff", 
				    	width: 4
				    }),
				    fill: new ol.style.Fill({
				    	color: color
				    })
					}),
			    /*text: new ol.style.Text({
	          text: size.toString(),
	          fill: new ol.style.Fill({
	            color: '#fff'
	          })
	        })*/
				});
				poiStyleCustom.getImage().getFill().setColor(color);
			}
			
			return poiStyleCustom;
		}
		else {
			return poiStyle;
		}
	}

	/*
	* Paint tracks
	*/
	if (mappingPaintTrack) {

		let trackLayer = new ol.layer.Vector({
	    title: 'Tracks',
	    type: 'track',
			source: trackSource,
			style: trackStyleFunction
	  });
		map.addLayer(trackLayer);
	}

	function trackStyleFunction(feature, resolution) {
		//if (resolution < resolutions[13]) {
		if (document.getElementById("paintLine").checked) {

			const styles = [
		    // linestring
				new ol.style.Style({
			    stroke: new ol.style.Stroke({ 
			    	color: "#ffcc33", 
			    	width: 4
			    }),
			    //geometry: feature.getGeometry().cspline(/*{
		        tension: 0.5, 
		        pointsPerSeg: 10,
						normalize: true
			    //}*/)
				})
			];

			let i = 1,
					count = 0;
			const geometry = feature.getGeometry();
			geometry.forEachSegment(function (start, end) {
				count++;
			});
			geometry.forEachSegment(function (start, end) {
				// only for last element
				if (i == count) {
					const dx = end[0] - start[0];
			    const dy = end[1] - start[1];
			    const rotation = Math.atan2(dy, dx);
			    // arrows
			    styles.push(
			      new ol.style.Style({
			        geometry: new ol.geom.Point(end),
			        image: new ol.style.Icon({
			          src: '/wp-content/themes/mapping/images/arrow.png',
			          anchor: [0.75, 0.5],
			          rotateWithView: true,
			          rotation: -rotation,
			        }),
			      })
			    );
			  }
		    i++;
		  });

	    return styles;
		}
	}

	/*
	* Show info in popup at mouseover
	*/
	var overlayPopup = new ol.Overlay({
		element: document.getElementById('popup'),
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	});
	map.addOverlay(overlayPopup);

	map.on('pointermove', function(evt) {
		let coordinate = evt.coordinate;
		let pixel = map.getPixelFromCoordinate(coordinate);
	    overlayPopup.setPosition(undefined);

	    // Cambiar flecha de ratón por mano
	    map.getTargetElement().style.cursor = map.hasFeatureAtPixel(evt.pixel, {
	    	layerFilter: function(layer) {
	        	return 	layer.get('type') === 'poi';
	        },
	        hitTolerance: 2
	    }) ? 'pointer' : 'grab';

	    let markup = "";
		map.forEachFeatureAtPixel(pixel, function(feature) {

			//console.log(feature.getProperties());
			if (feature.get("type") === "poi") {
				if (markup !== "") markup += "<br>";
				markup += feature.get("title");
				markup += "<br>Category: " + feature.get("category").post_title;
				if (feature.get("tag")) {
					markup += "<br>Etiquetas: ";
					feature.get("tag").forEach(function(tag, i) {
						if (i>0) markup += ', ';
						markup += tag.name;
					});
				}
			}

		    document.getElementById('popup').innerHTML = markup;
		    overlayPopup.setPosition(coordinate);
		}, {
	        layerFilter: function(layer) {
	        	return layer.get('type') === 'poi';
	        },
	        hitTolerance: 2
	    });
	});

	/*
	* Show content in window on click
	*/
	map.on('click', function(evt) {
		let coordinate = evt.coordinate;
		let pixel = map.getPixelFromCoordinate(coordinate);
	    
    let first = true;
		map.forEachFeatureAtPixel(pixel, function(feature) {

			// open content window
			if (first) {
				let link = document.createElement('a');
	        link.href = feature.get("permalink") + "?mapping_id=" + mapping_id;
	        document.body.appendChild(link);
	        link.click();

	        // zoom to district if itinerari
	        if (feature.get('type') === 'poi') {
	        	//console.log(feature.getGeometry().getCoordinates());
	        	map.getView().animate({ 
	        		center: feature.getGeometry().getCoordinates(), 
	        		zoom: 16 
	        	});
	        }
	        first = false;
		    }
		}, {
      layerFilter: function(layer) {
      	return 	layer.get('type') === 'poi';
      },
      hitTolerance: 2
	  });
	});
}

/*
* Show all mappings on home page
*/
function loadHome() {
	/*
	* Base map
	*/
	const view = new ol.View({
		center: ol.proj.fromLonLat([2.15, 41.40]),
		zoom: 12,
		minZoom: 12,
		maxZoom: 18,
		showFullExtent: true,
	  extent: extentBarcelona
	});

	const map = new ol.Map({
	    target: 'map',
	    layers: [
	    	coastLayer,
	    	districtsLayer,
	    	neighbourhoodsLayer,
	    	baseLayer,
	    ],
	    view: view
	});

	function markerStyleFunction(feature, resolution) {
		return new ol.style.Style({
      image: new ol.style.Icon(({
      	color: feature.get('color'),
        anchor: [0.5, 36],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: '/wp-content/plugins/acf-map-field/assets/images/marker_white.png'
      }))
    });
	}

	/*
	* add layer mappings
	*/
	let mappingLayer = new ol.layer.Vector({
    title: 'Mappings',
    type: 'mapping',
		source: new ol.source.Vector({
			format: new ol.format.GeoJSON(),
			url: dir + '/mappings-geojson/'
		}),
		style: markerStyleFunction
	});
	map.addLayer(mappingLayer);

	/*
	* Show info in popup at mouseover
	*/
	var overlayPopup = new ol.Overlay({
		element: document.getElementById('popup'),
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	});
	map.addOverlay(overlayPopup);

	map.on('pointermove', function(evt) {
		let coordinate = evt.coordinate;
		let pixel = map.getPixelFromCoordinate(coordinate);
	    overlayPopup.setPosition(undefined);

	    // Cambiar flecha de ratón por mano
	    map.getTargetElement().style.cursor = map.hasFeatureAtPixel(evt.pixel, {
	    	layerFilter: function(layer) {
	        	return 	layer.get('type') === 'mapping';
	        },
	        hitTolerance: 2
	    }) ? 'pointer' : 'grab';

	    let markup = "";
		map.forEachFeatureAtPixel(pixel, function(feature) {

			//console.log(feature.getProperties());
			if (feature.get("type") === "mapping") {
				if (markup !== "") markup += "<br>";
				markup += feature.get("title");
				if (feature.get("tags")) {
					markup += "<br>Etiquetas: ";
					feature.get("tags").forEach(function(tag, i) {
						if (i>0) markup += ', ';
						markup += tag.mapping_category.post_title;
					});
				}
			}

		  document.getElementById('popup').innerHTML = markup;
		  overlayPopup.setPosition(coordinate);
		}, {
	        layerFilter: function(layer) {
	        	return layer.get('type') === 'mapping';
	        },
	        hitTolerance: 2
	    });
	});

	/*
	* Show content in window on click
	*/
	map.on('click', function(evt) {
		let coordinate = evt.coordinate;
		let pixel = map.getPixelFromCoordinate(coordinate);

	  let first = true;
		map.forEachFeatureAtPixel(pixel, function(feature) {

			// open content window
			if (first) {
				window.open(feature.get("permalink"), "_self");
        first = false;
    	}
		}, {
      layerFilter: function(layer) {
      	return 	layer.get('type') === 'mapping';
      },
      hitTolerance: 2
    });
	});
}