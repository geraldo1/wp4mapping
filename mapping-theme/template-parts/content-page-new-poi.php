<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mapping
 */

acf_enqueue_scripts();

if (isset($_GET['post_id'])) {
	$post_id = $_GET['post_id'];

	// check if POI does exist
	$response = file_get_contents('https://mapping.xrcb.cat/wp-json/wp/v2/poi/'.$post_id);
}

if (isset($_GET['mapping_id'])) {
	$mapping_id = $_GET['mapping_id'];

	// check if mapping does exist
	$response = file_get_contents('https://mapping.xrcb.cat/wp-json/wp/v2/mapping/'.$mapping_id);
	$response = json_decode($response);
	$mapping_title = $response->title->rendered;
}

// check if mapping_id is valid
if (!$response): 
?>
	<article>
		<header class="entry-header">
			<h1 class="entry-title">id no válida</h1>
		</header>
	</article>

<?php 
	return; 
endif;

// only show form if editing existing POI or adding new POI for a defined mapping
if (isset($post_id) || isset($mapping_id)):
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php mapping_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		// frontend form
		$url = '%post_url%';
		$fields = array(
			'category',
	    	'image',
	    	'map',
	    	'coordinates',
			'mapping',
	    );

		if (isset($mapping_id)) {
			$url .= '?mapping_id=' . $mapping_id;

			// show audio field
			if (get_field('add_audio', $mapping_id))
				array_splice($fields, 2, 0, 'audio');

			// show tags field
			if (get_field('has_tags', $mapping_id))
				array_splice($fields, 2, 0, 'poi_tags');
		}

		$options = array(
		    'fields' => $fields,
		    'post_title' => true,
		    'post_content' => true,
		    'uploader' => 'basic',
		    'submit_value' => __('Publicar', 'xrcb'),
		    'return' => $url,
		);

		if (isset($post_id)) {
			// edit existing POI
			$options['post_id'] = $post_id;
		}
		else {
			// create new POI
			$options['post_id'] = 'new_post';
		    $options['new_post'] = array(
	            'post_type' => 'poi',
	            'post_status' => 'publish'
	        );
		}
		
		acf_form( $options );

		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php else: ?>

<article>
	<header class="entry-header">
		<h1 class="entry-title">Info no válida</h1>
	</header>
</article>

<?php endif; ?>

<script>
(function($) {
	// fill mapping with mapping_id and hide
	<?php
		if (isset($mapping_id)):
	?>
	    let data = {
		  "id": "<?php echo $mapping_id; ?>",
		  "text": "<?php echo $mapping_title; ?>"
	    };
	    let option = new Option(data.text, data.id);
	    $('#acf-field_659d753ce1030').append(option).trigger('change');
	    $('#acf-field_659d753ce1030').trigger({
	        type: 'select2:select',
	        params: {
	            data: data
	        }
	    });
	<?php
		endif;
	?>
    $('.acf-field-659d753ce1030').hide();
})(jQuery);
</script>