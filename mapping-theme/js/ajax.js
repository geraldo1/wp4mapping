/*
	based on Tutorial "How To AJAXify WordPress Theme"
	https://deluxeblogtips.com/how-to-ajaxify-wordpress-theme/
*/

jQuery(document).ready(function($) {

	// capture click on home mapping to redirect
	$('.articles a.gotomapping').click(function(evt) {
		document.location.href = evt.currentTarget.href;
	});

    let $mainContent = $("#primary"),
    	siteUrl = window.location.protocol.toString() + "//" + window.location.host.toString(),
    	url = '';

	$(document).on("click", "a[href^='"+siteUrl+"']:not([href*='/wp-admin/']):not([href*='/wp-login.php']):not([href$='/feed/']):not([href*='/wp-content/uploads/']):not([href*='/wp-content/plugins/']):not([href*='/new-poi'])", function(e) {
		//console.log(e.currentTarget.search, this.pathname);

		if (e.currentTarget.search)
			location.hash = this.pathname + e.currentTarget.search;
		else
			location.hash = this.pathname;

		return false;
	});

    /*$("#searchform").submit(function(e) {
		location.hash = '?s=' + $("#s").val();
		e.preventDefault();
    });*/

    $(window).bind('hashchange', function(){

    	url = window.location.hash.substring(1);

		//exclude URLs from ajax behavior
		if (!url ||
			url.indexOf("comment")!= -1 ||
			url.indexOf("respond")!= -1 ||
			url.indexOf("json")!= -1) {
			return;
		}

		url = url + " #content";

		$mainContent.animate({opacity: "0.1"}).html('<img class="loader" src="' + dir + '/wp-content/themes/mapping/images/ajax-loader.gif" />').load(url, function(data) {
			$mainContent.animate({opacity: "1"});

			var oPageInfo = {
	            title: data.substring(data.indexOf("<title>")+7,data.indexOf("</title>")),
	            url: window.location.origin+window.location.hash.substring(1)
	        }
		    //console.log(window.history.state, oPageInfo);

		    window.history.replaceState(oPageInfo, oPageInfo.title, oPageInfo.url);
		    //console.log(window.history.state);

		    //datatable pois
			if (url.indexOf("/lista-puntos")!= -1) {
				//load js
				$.getScript("https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js")
				.done(function(script, textStatus) {
					//apply data table
					$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="poi-table"></table>');
					var datatable = $('#poi-table').DataTable({
						paging: true,
						pageLength: 10,
						info: false,
						responsible: true,
						searching: false,	// to do! pass search param with ajx call and filter it at server side
						stateSave: false,
						serverSide: true,
						ajax: function ( data, callback, settings ) {
							$.ajax({
				                url: '/wp-json/wp/v2/poi',
				                type: 'get',
				                contentType: 'application/x-www-form-urlencoded',
				                data: {
				                    offset: data.start,
				                    per_page: data.length
				                },
				                success: function( data, textStatus, jQxhr ){
				                	let totalRecords = jQxhr.getResponseHeader('x-wp-total');
				                    callback({
				                        data: data,
				                        recordsTotal: totalRecords,
				                        recordsFiltered: totalRecords
				                    });
				                },
				                error: function( jqXhr, textStatus, errorThrown ){
				                }
				            });
				        },
						columns: [
							{ "data": "title.rendered", "title" : "Título", "className" : "name", "render": function ( data, type, row ) {
									return "<a class='btn-poi' href='"+row["link"]+"' data-lat='"+row["acf"].coordinates.latitude+"' data-lon='"+row["acf"].coordinates.longitude+"'>"+data+"</a>";
								},
							},
							{ "data": "acf.category", "title" : "Category", "className" : "category", "render": function ( data, type, row, meta ) {
									let currentCell = $("#poi-table").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
									$.getJSON( '/wp-json/wp/v2/mapping-category/' + data, function(category) {
								  		$(currentCell).html("<span class='"+category.slug+"' style='background-color:"+category.acf.color+"'>"+category.title.rendered+"</span>");
									});
									return null;
								}
							},
							{ "data": "acf.poi_tags", "title" : "Etiqueta", "className" : "tag", "render": function ( data, type, row, meta ) {
									let currentCell = $("#poi-table").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);
									if (data.length > 0)
										$.getJSON( '/wp-json/wp/v2/poitag/' + data, function(tag) {
									  		$(currentCell).html("<span class='"+tag.slug+"'>"+tag.name+"</span>");
										});
									return null;
								}
							},
							{ "data": "date", "title" : "Fecha", "className" : "date", "render": function ( data, type, row ) { 
									return new Date(data).toLocaleDateString('es-ES', {
										year: 'numeric',
										month: 'numeric',
										day: '2-digit',
									}) 
								} 
							},
							{ "data": "acf.coordinates.latitude", "title" : "Latitude", "className" : "lat" },
							{ "data": "acf.coordinates.longitude", "title" : "Longitude", "className" : "lon" }
						],
					}).on( 'init.dt', function () {

		    			/*$("a.btn-poi").click(function(event){
					        //event.preventDefault();
					        map.getView().animate({ center: ol.proj.fromLonLat([$(this).data("lon"), $(this).data("lat")]), zoom: 16 });
					    });*/
					});
				});
			}

		    //single itinerari, parada or espai
			/*else if (url.indexOf("/itinerari/")!= -1 ||
				url.indexOf("/parada/")!= -1 ||
				url.indexOf("/espai/")!= -1) {

				$('body').removeClass('page-id-21');

				console.log(url);
			}

			else {
				$('#primary').addClass('standard');
				$('p').addClass('standard');
			}*/

		    // go back in history
			$(".back-button").click(function() {
				window.history.back();
			});
		});
    });
    $(window).trigger('hashchange');

    // go back in history
	$(".back-button").click(function() {
		window.history.back();
	});
});

window.onpopstate = function(event) {

	if (event.state) {
		var host = "https://"+location.hostname;
		var newHash = event.state.url.substring(host.length);

		//console.log(event.state, location.hash, newHash);

		// !!! without next line location history seems to be right
		location.hash = newHash;
	}

	//console.log(window.history.length);
};

window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};
