<?php
/**
	Template Name: Mappings GeoJSON
*/

header('Content-Type: application/json; charset=utf-8');
//header('Content-Disposition: attachment; filename='export.csv'');
$fp = fopen('php://output', 'w');

$features = [];

$query_args = array(
	'post_type' => 'mapping',
	'post_status' => 'publish',
	'posts_per_page' => -1
);

$my_query = new WP_Query($query_args);

if ( $my_query->have_posts() ) {

	$features = array(
		'features' => array(),
		'type' => 'FeatureCollection'
	);

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$features['features'][] = array(
			'id' => (int)get_the_ID(),
			'properties' => array(
				'id' => (int)get_the_ID(),
				'title' => get_the_title(),
				'tags' => get_field('tags'),
				'color' => get_field('color'),
				'type' => 'mapping',
				'permalink' => get_permalink(get_the_ID()),
			),
			'type' => 'Feature',
			'geometry' => array(
				'type' => 'Point',
				'coordinates' => array(
					(float)get_field('coordinates')['longitude'],
					(float)get_field('coordinates')['latitude']
				)
			)
		);
	}
}

echo json_encode($features);

fclose($fp);

?>