<?php
/**
 * Template Name: POIs Form
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mapping
 */

if (isset($_GET['mapping_id'])) {
	$mapping_id = $_GET['mapping_id'];
	wp_localize_script('map-js', 'map_js_vars', array('mapping_id' => $mapping_id));
}

// limit to logged in users
if (!is_user_logged_in() && !( current_user_can( 'administrator' ) || current_user_can( 'editor' ) || current_user_can( 'author' ) ))
	wp_redirect(wp_login_url(get_permalink()));

acf_form_head();

get_header();
?>

	<div id="primary" class="content-area page">

		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page-new-poi' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</div>
	</div>

<?php
get_footer();
